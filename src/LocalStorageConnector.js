export const getBooks = () => {
    if (!localStorage.getItem("books")) {
        localStorage.setItem("books", JSON.stringify(originData));
    }
    return JSON.parse(localStorage.getItem("books"));
}

export const getBooksInPriceRange = (fromPrice, toPrice) => {
    return getBooks().filter(book => book.price >= fromPrice && book.price <= toPrice);
}

export const getLowestPrice = () => {
    return Math.min(...getBooks().map(book => Number(book.price)));

}

export const getHighestPrice = () => {
    return Math.max(...getBooks().map(book => Number(book.price)));
}

export const setBooks = (books) => {
    localStorage.setItem("books", JSON.stringify(books));
}

const originData = [
    {
        "id": 1,
        "isbn": "9780007136582",
        "name": "The Lord Of The Rings",
        "author": "Tolkien J.R.R.",
        "price": "49",
        "description": "The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil.",
        "image": true,
        "imageUrlS": "https://covers.openlibrary.org/b/isbn/9780007136582-S.jpg",
        "imageUrlM": "https://covers.openlibrary.org/b/isbn/9780007136582-M.jpg",
        "imageUrlL": "https://covers.openlibrary.org/b/isbn/9780007136582-L.jpg"
    },
    {
        "id": 2,
        "isbn": "9780553573404",
        "name": "A Game of Thrones : A Song of Ice and Fire: Book One - Hardcover",
        "author": "Martin George R.R.",
        "price": "10",
        "description": "The first novel in a brand-new epic fantasy trilogy by the author of Fevre Dream and The Armageddon Rag. Long ago, a mysterious event threw the seasons out of balance. The golden summers go on for yea",
        "image": true,
        "imageUrlS": "https://covers.openlibrary.org/b/isbn/9780553573404-S.jpg",
        "imageUrlM": "https://covers.openlibrary.org/b/isbn/9780553573404-M.jpg",
        "imageUrlL": "https://covers.openlibrary.org/b/isbn/9780553573404-L.jpg"
    },
    {
        "id": 3,
        "isbn": "9780747532743",
        "name": "Harry Potter and the Philosopher's Stone",
        "author": "Rowling, J. K.",
        "price": "11",
        "description": "Harry Potter and the Philosopher's Stone was J.K. Rowling's first novel, followed by the subsequent six titles in the Harry Potter series, as well as three books written for charity: Fantastic Beasts ",
        "image": true,
        "imageUrlS": "https://covers.openlibrary.org/b/isbn/9780747532743-S.jpg",
        "imageUrlM": "https://covers.openlibrary.org/b/isbn/9780747532743-M.jpg",
        "imageUrlL": "https://covers.openlibrary.org/b/isbn/9780747532743-L.jpg"
    }
]