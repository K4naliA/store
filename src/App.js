import './App.css';
import PageTopHeader from "./components/PageTopHeader";
import BooksTable from "./components/BooksTable";
import Container from "react-bootstrap/Container";

function App() {
  return (
    <div className="App">
        <Container>
            <PageTopHeader/>
            <BooksTable/>
        </Container>
    </div>
  );
}

export default App;
