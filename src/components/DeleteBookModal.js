import {Modal, Button, Form} from "react-bootstrap";

const DeleteBookModal = (props) => {

    const onYes = () => {
      let books = JSON.parse(localStorage.getItem("books")).filter(book => book.id !== props.bookId);
      console.log(books);
      localStorage.setItem("books", JSON.stringify(books));
    };

    return (
        <Modal show={true}>
            <Form onSubmit={onYes}>
                <Modal.Header>
                    <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.onNo}>No</Button>
                    <Button variant="primary" type="submit">Yes</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    );
}
export default DeleteBookModal;