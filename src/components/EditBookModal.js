import {useState} from "react";
import {Modal, Button, Form} from "react-bootstrap";
import CurrencyInput from 'react-currency-input-field';
import {getBooks, setBooks} from '../LocalStorageConnector';

const EditBookModal = (props) => {
    const [isbn, setIsbn] = useState(props.book.isbn);
    const [name, setName] = useState(props.book.name);
    const [author, setAuthor] = useState(props.book.author);
    const [price, setPrice] = useState(props.book.price);
    const [description, setDescription] = useState(props.book.description);
    const [image, setImage] = useState(props.book.image);

    const onSubmit = () => {
        let book = {
            id: props.book.id,
            isbn: isbn,
            name: name,
            author: author,
            price: price,
            description: description,
            image: image,
            imageUrlS: `https://covers.openlibrary.org/b/isbn/${isbn}-S.jpg`,
            imageUrlM: `https://covers.openlibrary.org/b/isbn/${isbn}-M.jpg`,
            imageUrlL: `https://covers.openlibrary.org/b/isbn/${isbn}-L.jpg`
        };
        let books = getBooks();
        let bookIndex = books.findIndex(book => book.id === props.book.id);
        books[bookIndex] = book;
        setBooks(books);
    }

    return (
        <Modal show={true}>
            <Form onSubmit={onSubmit}>
                <Modal.Header>
                    <Modal.Title>Edit book</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group className="mb-3" controlId="isbn">
                        <Form.Label>ISBN</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Enter ISBN"
                            value={isbn}
                            onInput={(e) => setIsbn(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="bookName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Enter name"
                            value={name}
                            onInput={(e) => setName(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="author">
                        <Form.Label>Author</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Enter author"
                            value={author}
                            onInput={(e) => setAuthor(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="bookPrice">
                        <Form.Label>Price</Form.Label>
                        <CurrencyInput
                            id="edit-book-price"
                            name="edit-book-price"
                            className="form-control"
                            prefix="$"
                            placeholder="Enter price"
                            defaultValue={price}
                            decimalsLimit={2}
                            allowNegativeValue={false}
                            onValueChange={(value, name) => setPrice(value)}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="bookDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows={3} value={description}
                            onInput={(e) => setDescription(e.target.value)}
                            maxLength="200"
                        />
                        <Form.Text className="text-muted">
                            200 characters
                        </Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="bookImage">
                        <Form.Check type="checkbox" label="Search for image" defaultChecked={image} onChange={() => setImage(!image)}/>
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={props.onCancel}>Cancel</Button>
                    <Button variant="primary" type="submit">Save</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    );
}
export default EditBookModal;