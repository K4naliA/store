import React, {useState} from "react";
import EditBookModal from "./EditBookModal";
import DeleteBookModal from "./DeleteBookModal";
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

const BookLineItem = (props) => {
    const [showEditBookModal, setEditBookModal] = useState(false);
    const [showDeleteBookModal, setDeleteBookModal] = useState(false);

    const toggleEditBookModal = () => {
        setEditBookModal(!showEditBookModal);
    };

    const toggleDeleteBookModal = () => {
        setDeleteBookModal(!showDeleteBookModal);
    };

    return (
        <>
            {showEditBookModal && <EditBookModal book={props.book} onCancel={toggleEditBookModal}></EditBookModal>}
            {showDeleteBookModal && <DeleteBookModal bookId={props.book.id} onNo={toggleDeleteBookModal}></DeleteBookModal>}
            <div>
                <Container>
                    <Row>
                        <Col xs={6} md={4}>
                            { props.book.image && <Image src={props.book.imageUrlM} thumbnail />}
                        </Col>
                        <Col xs={6} md={4}>
                            <h2>{props.book.name}</h2>
                            <h5>ISBN: {props.book.isbn}</h5>
                            <h5>Author: {props.book.author}</h5>
                            <h5>Price: {props.book.price}</h5>
                            <h5>Description:</h5>
                            <p>{props.book.description}</p>
                            <ButtonGroup>
                                <Button variant="success" onClick={toggleEditBookModal}>Edit</Button>
                                <Button variant="danger" onClick={toggleDeleteBookModal}>Delete</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                    <hr/>
                </Container>
            </div>
        </>
    );
}
export default BookLineItem;