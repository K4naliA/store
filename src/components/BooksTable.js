import {useState, useEffect} from "react";
import AddBookModal from "./AddBookModal";
import BookLineItem from "./BookLineItem";
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Alert from 'react-bootstrap/Alert';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import CurrencyInput from 'react-currency-input-field';
import {getBooks, getBooksInPriceRange, getLowestPrice, getHighestPrice} from '../LocalStorageConnector';

const BooksTable = () => {
    const [showAddBookModal, setShowAddBookModal] = useState(false);
    const [showPriceAlert, setShowPriceAlert] = useState(false);
    const [priceAlertMessage, setPriceAlertMessage] = useState('');
    const [fromPrice, setFromPrice] = useState(getLowestPrice());
    const [toPrice, setToPrice] = useState(getHighestPrice());
    const [booksToShow, setBooksToShow] = useState(getBooks());
    const [sortType, setSortType] = useState('none');

    const convertToLineItems = (books) => {
        return books.map((book) => <BookLineItem key={book.id} book={book}/>);
    }

    const toggleAddBookModal = () => {
        setShowAddBookModal(!showAddBookModal);
    };

    const sort = (eventKey) => {
        const booksInPriceRange = getBooksInPriceRange(parseFloat(fromPrice), parseFloat(toPrice));
        let books;
        switch (eventKey) {
            case 'none':
                books = [...booksInPriceRange].sort((b1, b2) => b1.id - b2.id);
                setBooksToShow(books);
                break;
            case 'priceLH':
                books = [...booksInPriceRange].sort((b1, b2) => b1.price - b2.price);
                setBooksToShow(books);
                break;
            case 'priceHL':
                books = [...booksInPriceRange].sort((b1, b2) => b2.price - b1.price);
                setBooksToShow(books);
                break;
            case 'nameAZ':
                books = [...booksInPriceRange].sort((b1, b2) => b1.name.localeCompare(b2.name));
                setBooksToShow(books);
                break;
            case 'nameZA':
                books = [...booksInPriceRange].sort((b1, b2) => b2.name.localeCompare(b1.name));
                setBooksToShow(books);
                break;
            case 'authorAZ':
                books = [...booksInPriceRange].sort((b1, b2) => b1.author.localeCompare(b2.author));
                setBooksToShow(books);
                break;
            case 'authorZA':
                books = [...booksInPriceRange].sort((b1, b2) => b2.author.localeCompare(b1.author));
                setBooksToShow(books);
                break;
            default:
                break;
        }
    };

    const searchInPriceRange = () => {
        if (fromPrice === undefined) {
            setPriceAlertMessage("Price 'from' is empty.");
            setShowPriceAlert(true);
        } else if (toPrice === undefined) {
            setPriceAlertMessage("Price 'to' is empty.");
            setShowPriceAlert(true);
        } else if (parseFloat(fromPrice) > parseFloat(toPrice)) {
            setPriceAlertMessage("Price 'from' cannot be greater than 'to'.");
            setShowPriceAlert(true);
        } else {
            setShowPriceAlert(false);
            sort(sortType);
        }
    };

    let lineItems = convertToLineItems(booksToShow);

    useEffect(() => {
        sort(sortType);
    }, [sortType]);

    return (
        <>
            {showAddBookModal && <AddBookModal onCancel={toggleAddBookModal}></AddBookModal>}
            {showPriceAlert && <Alert variant="danger">{priceAlertMessage}</Alert>}
            <Row >
                <Col xs md lg="2">
                    <CurrencyInput
                        id="from-price"
                        name="from-price"
                        className="form-control"
                        prefix="$"
                        placeholder="from"
                        defaultValue={fromPrice}
                        value={fromPrice}
                        decimalsLimit={2}
                        allowNegativeValue={false}
                        onValueChange={(value, name) => setFromPrice(value)}
                    />
                </Col>
                <Col xs md lg="2">
                    <CurrencyInput
                        id="to-price"
                        name="to-price"
                        className="form-control"
                        prefix="$"
                        placeholder="to"
                        defaultValue={toPrice}
                        value={toPrice}
                        decimalsLimit={2}
                        allowNegativeValue={false}
                        onValueChange={(value, name) => setToPrice(value)}
                    />
                </Col>
                <Col xs md lg="2">
                    <ButtonGroup>
                        <Button variant="primary" onClick={searchInPriceRange}>Search</Button>
                        <DropdownButton as={ButtonGroup} title="Sort By" disabled={showPriceAlert} onSelect={(eventKey, event) => setSortType(eventKey)}>
                            <Dropdown.Item eventKey="none"></Dropdown.Item>
                            <Dropdown.Item eventKey="priceLH">Price: Low to High</Dropdown.Item>
                            <Dropdown.Item eventKey="priceHL">Price: High to Low</Dropdown.Item>
                            <Dropdown.Item eventKey="nameAZ">Name: A-Z</Dropdown.Item>
                            <Dropdown.Item eventKey="nameZA">Name: Z-A</Dropdown.Item>
                            <Dropdown.Item eventKey="authorAZ">Author: A-Z</Dropdown.Item>
                            <Dropdown.Item eventKey="authorZA">Author: Z-A</Dropdown.Item>
                        </DropdownButton>
                    </ButtonGroup>
                </Col>
                <Col xs md lg="2">
                    <Button variant="primary" onClick={toggleAddBookModal}>New Book</Button>
                </Col>
            </Row>
            <hr/>
            {lineItems}
        </>
    );
}
export default BooksTable;