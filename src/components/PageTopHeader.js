const PageTopHeader = () => {
    return (
      <header>
          <h1>Book Management System</h1>
          <hr/>
      </header>
    );
}
export default PageTopHeader;